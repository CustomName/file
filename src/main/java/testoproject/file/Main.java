package testoproject.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String... args) throws IOException {
        File file = new File("src\\main\\resources\\data.txt");

        String srcData = readFileToStr(file);
        System.out.println("srcData: " + srcData);

        List<Integer> sortNatural = sort(srcData, Comparator.naturalOrder());
        System.out.println("sortNatural: " + sortNatural);

        List<Integer> sortReverse = sort(srcData, Comparator.reverseOrder());
        System.out.println("sortReverse: " + sortReverse);
    }

    private static String readFileToStr(File file) throws IOException{
        if(!file.exists()) return "";

        BufferedReader reader = new BufferedReader(new FileReader(file));
        StringBuilder sb = new StringBuilder();
        String line = reader.readLine();
        while (line != null) {
            sb.append(line);
            line = reader.readLine();
        }

        reader.close();
        return sb.toString();
    }

    private static List<Integer> sort(String data, Comparator<? super Integer> comparator){
        return Stream.of(data.split(","))
                .map(Integer::new)
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}
